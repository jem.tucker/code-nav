psycopg2>=2.7,<2.8
dj-database-url>=0.4,<0.5
Whitenoise>=3.3,<3.4
Gunicorn>=19.7,<19.8
Django>=1.10,<1.11
GitPython>=2.1,<2.2
teager==0.1.5
