from os import path
from glob import glob
from git import Repo
from browser.models import ProjectFile, Symbol

import subprocess
import shutil
import teager
import logging


class IndexedProject(object):
    """
    Class representing a fully cloned / indexed representation of a project on
    disk.
    """

    def __init__(self, proj):
        """
        Initialises a project using the passed Project metadata. If the project
        does not exist on disk then this will clone and index it before
        returning.
        @param proj: The Project instance to index.
        """
        self.proj = proj

        try:
            if not path.isdir(self.proj.repository_path):
                self._clone()
            self._index()
        except Exception:
            # Cleanup on failure
            if path.isdir(self.proj.repository_path):
                shutil.rmtree(self.proj.repository_path)
            raise

    def path(self):
        """
        Get the path to this project
        @return: The full repository path
        """
        return self.proj.repository_path

    def relative_path(self, filepath):
        """
        Return the relative path to a file from the project root.
        @param filepath: The full file path
        @return: A relative file path from self.proj.repository_path
        """
        return filepath.replace(self.proj.repository_path + '/', '')

    def read(self, proj_file):
        """
        Read the contents of a file from this project
        @return: The contents of a file
        """
        full_path = path.join(self.proj.repository_path, proj_file.path)
        with open(full_path, 'r') as f:
            return f.read()

    def _clone(self):
        """
        Clone a project's git repository into a temporary directory
        """
        logging.info(
            'Cloning project "%s" into "%s"', 
            self.proj.repository_url, 
            self.proj.repository_path)
        
        repo = Repo.clone_from(
            self.proj.repository_url,
            self.proj.repository_path
        )

    def _callback(self, symlist):
        """
        Returns a callback to be used by teager when parsing a tu. Callback
        creates a DB entry for each symbol found. 
        """
        def cb(symbol=None, filename=None, lineno=None, symtype=None):
            if symbol and filename and lineno and symtype:
                if not filename.startswith(self.proj.repository_path):
                    logging.debug(
                        'Ignored Symbol: %s:%s:%d', 
                        symbol, 
                        filename, 
                        lineno)
                    return

                logging.debug('Symbol: %s:%s:%d', symbol, filename, lineno)
                symlist.append({
                    'name': symbol, 
                    'line': lineno,
                    'file': filename,
                    'type': symtype})
        return cb 
    
    def _index_translation_unit(self, proj_file):
        """
        Index a single translation unit 
        """
        fullpath = path.join(self.proj.repository_path, proj_file.path) 
        
        logging.info('Indexing TU: %s', fullpath)

        symlist = []
        teager.parse(fullpath, self._callback(symlist))    

        for s in symlist:
            pf = proj_file 
            filepath = s['file']
            relpath = filepath.replace(self.proj.repository_path, '', 1)
            if filepath != fullpath:
                with open(filepath, 'r') as f:
                    pf, created = ProjectFile.objects.get_or_create(
                        name=path.basename(filepath),
                        path=relpath, 
                        project=self.proj)
                    if created:
                        pf.data = f.read()
                        pf.save()
            
            _ = Symbol.objects.create(
                name=s['name'],
                line=s['line'],
                symtype=s['type'],
                proj_file=pf)

    def _index(self):
        """
        Indexes a project creating ProjectFile and Symbol instances in the
        database. 
        """
        files = self._raw_files()
        
        logging.info(
            'Indexing %d for project "%s"', 
            len(files), 
            self.proj.repository_name)

        # Create a ProjectFile instance for every c/h file and index each 
        # translation unit with teager
        for f in files:
            logging.debug('ProjectFile: %s', f)
            with open(path.join(self.proj.repository_path, f), 'r') as fd:
                p, _ = ProjectFile.objects.get_or_create(
                    name=path.basename(f),
                    path=f,
                    project=self.proj,
                    data=fd.read())
            if f.endswith('c'):
                self._index_translation_unit(p)

    def _raw_files(self):
        """
        Lists all .h and .c files in this project
        @return List of all .h and .c files under self.repository_path
        """
        h_path = self.proj.repository_path + '/**/*.h'
        c_path = self.proj.repository_path + '/**/*.c'
        h_files = [f for f in glob(h_path, recursive=True)]
        c_files = [f for f in glob(c_path, recursive=True)]
        return [f.replace(self.proj.repository_path + '/', '')
                for f in h_files + c_files]
