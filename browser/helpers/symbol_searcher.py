from subprocess import Popen, PIPE
from .indexed_project import IndexedProject

import shlex
import traceback


class SymbolSearcher(object):
    """
    Helper class that provides a search service for indexed projects.
    """

    def __init__(self, proj):
        """
        Create a searcher for the given Project
        @param proj: The Project to search
        """
        self.proj = IndexedProject(proj)

    def find(self, symbol):
        """
        Find a given symbol.
        This will search the CScope database (in a new process) and return a
        list of files that contain the symbol.
        @param symbol: The string symbol to search for
        @return: List of (file, line number) pairs
        """
        # Run a cscope search within the project directory
        working_dir = self.proj.path()
        command_line = "cscope -d -L1%s" % symbol
        args = shlex.split(command_line)

        with Popen(args, cwd=working_dir, stdout=PIPE ) as proc:
            output = []

            # Process the output. Lines will be in the form:
            #   <filepath> <symbol> <line num> <line>
            for line in proc.stdout:
                try:
                    splitline = line.decode().split()
                    filepath = self.proj.relative_path(splitline[0])
                    linenum = splitline[2]
                    output.append((filepath, linenum))
                except Exception:
                    traceback.print_exc()

            return output
