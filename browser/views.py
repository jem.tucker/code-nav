from django.http import HttpResponse, HttpResponseBadRequest, \
    HttpResponseNotFound, JsonResponse
from django.utils.html import escape
from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie

from browser.models import Project, ProjectFile, Symbol 

import json
import os
import traceback


@ensure_csrf_cookie
def home(request):
    """
    Renders the home page and ensures a csrf cookie is set
    @return: HttpResponse of the main browser page.
    """
    return render(request, 'cb_index.html')


def projects(request):
    """
    Loads a project. This will clone and index one if not already done.
    @param request: GET with an 'id'
    @return: If successful, an HttpResponse instance containing a Project as a
             JSON object
    """
    if request.method == 'GET':
        # Retrieve a project as JSON (or all)
        return _retrieve(request)

    return HttpResponseBadRequest()


def _retrieve(request):
    """
    Retrieves an existing project with 'id' or all projects if no id is provided
    @param request: GET with an optional 'id'
    @return: If successful, an HttpResponse instance containing a Project as a
             JSON object or an array of all projects
    """
    proj_id = request.GET.get('id')
    if proj_id:
        try:
            proj = Project.objects.get(id=proj_id)
            return JsonResponse(proj.as_dict())
        except Project.DoesNotExist:
            return HttpResponseNotFound()
    else:
        projects = [ p.as_dict() for p in Project.objects.all()]
        return HttpResponse(
            json.dumps(projects),
            content_type="application/json"
        )


def read(request):
    """
    Read a file from a project
    @param request: GET containing the 'id'
    @return: HTTP escaped file contents
    """
    if request.method != 'GET':
        return HttpResponseBadRequest()

    try:
        file_id = request.GET['id']
        proj_file = ProjectFile.objects.get(id=file_id)
        data = proj_file.data 
        return HttpResponse(data)
    except Exception:
        traceback.print_exc()
        return HttpResponseBadRequest()


def find(request):
    """
    Find the index of a file containing a given symbol within a specified
    project.
    @param request: GET containing the project 'id' and 'symbol' string
    @return: JSON contining a file object
    """
    if request.method != 'GET':
        return HttpResponseBadRequest()

    try:
        proj_id = request.GET['id']
        symbol = request.GET['symbol']
        
        result = Symbol.find(proj_id, symbol)
        if result:
            return JsonResponse({ 
                'file': result.proj_file.as_dict(), 
                'line': result.line })
        else:
            # No files were found. Return error
            return HttpResponseNotFound()

    except Exception:
        traceback.print_exc()
        return HttpResponseBadRequest()
