from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    url(r'^projects/$', views.projects),
    url(r'^file/$', views.read),
    url(r'^find/$', views.find)
]
