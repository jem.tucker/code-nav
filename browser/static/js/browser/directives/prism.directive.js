(function() {
  'use strict';

  angular
    .module('browser.directives')
    .directive('prism', prism);

    /**
    * @namespace prism
    */
    function prism() {

      /**
      * @name directive
      * @desc The directive to be returned
      */
      var directive = {
        restrict: 'A',
        scope: {
          source: '@'
        },
        controller: 'PrismController',
        controllerAs: 'vm',
        // link: linkFn,
        template: "<code ng-bind='source'></code>"
      };

      return directive;
    }
})();
