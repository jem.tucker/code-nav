(function () {
  'use strict';

  angular
    .module('browser.services')
    .factory('ProjectFile', ProjectFile);

  ProjectFile.$inject = ['$http'];

  /**
  * @namespace ProjectFile
  * @returns {Factory}
  */
  function ProjectFile($http) {
    var ProjectFile = {
      getContent: getContent
    };

    return ProjectFile;

    //////////////////////

    /**
     * @name getContent
     * @desc Try to get the content of a ProjectFile by id
     * @param {integer} id The ProjectFile id
     * @returns {Promise}
     */
    function getContent(id) {
      return $http.get('/browser/file/?id=' + id).then(successFn);

      /**
       * @name successFn
       * @desc Redirect to index and populate the currentProject
       */
      function successFn(data, status, headers, config) {
        ProjectFile.content = data.data;
      }
    }
  }
})();
