(function () {
  'use strict';

  angular
    .module('browser.services')
    .factory('Symbol', Symbol);

  Symbol.$inject = ['$http'];

  /**
  * @namespace Symbol
  * @returns {Factory}
  */
  function Symbol($http) {
    var Symbol = {
      search: search
    };

    return Symbol;

    //////////////////////

    /**
     * @name search
     * @desc Try to get the file id for a file containing symbol 'sym'
     * @param {integer} proj The Project id to search
     * @param {integer} sym The symbol to search for
     * @returns {Promise}
     */
    function search(proj, sym) {
      Symbol.name = sym;
      Symbol.proj = proj;
      var url = '/browser/find/?id=' + proj + '&symbol=' + sym;
      return $http.get(url).then(successFn);

      /**
       * @name successFn
       * @desc Redirect to index and populate the currentProject
       */
      function successFn(data, status, headers, config) {
        Symbol.file = data.data.file;
        Symbol.line = data.data.line;
      }
    }
  }
})();
