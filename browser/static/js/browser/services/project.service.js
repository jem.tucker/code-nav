(function () {
  'use strict';

  angular
    .module('browser.services')
    .factory('Project', Project);

  Project.$inject = ['$http'];

  /**
  * @namespace Project
  * @returns {Factory}
  */
  function Project($http) {
    var Project = {
      create: create,
      all: all,
      get: get
    };

    return Project;

    //////////////////////

    /**
     * @name create
     * @desc Try to create a new project
     * @param {string} url The repository url
     * @returns {Promise}
     */
    function create(url) {
      return $http.post('/browser/projects/', {
        url: url
      }).then(successFn);

      /**
       * @name successFn
       * @desc Redirect to index and populate the currentProject
       */
      function successFn(data, status, headers, config) {
        Project.current = data.data;
      }
    }

    /**
     * @name all
     * @desc Try to get an array of all projects
     * @returns {Promise}
     */
    function all() {
      return $http.get('/browser/projects/').then(successFn);

      /**
       * @name successFn
       * @desc Redirect to index and populate the projects list
       */
      function successFn(data, status, headers, config) {
        Project.projects = data.data;
      }
    }

    /**
     * @name get
     * @desc Try to get a single project with id='id'
     * @param {integer} id The project id
     * @returns {Promise}
     */
    function get(id) {
      return $http.get('/browser/projects/?id=' + id).then(successFn);

      /**
       * @name successFn
       * @desc Redirect to index and populate the currentProject
       */
      function successFn(data, status, headers, config) {
        Project.current = data.data;
      }
    }
  }
})();
