(function () {
  'use strict';

  angular
    .module('browser', [
      'browser.controllers',
      'browser.services',
      'browser.config',
      'browser.routes',
      'browser.directives'
    ]);

  angular
    .module('browser.controllers', []);

  angular
    .module('browser.services', []);

  angular
    .module('browser.config', []);

  angular
    .module('browser.routes', ['ngRoute']);

  angular
    .module('browser.directives', []);
})();
