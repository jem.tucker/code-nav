(function() {
  'use strict';

  angular
    .module('browser.controllers')
    .controller('BrowserAllController', BrowserAllController);

  BrowserAllController.$inject = ['$location', '$scope', 'Project'];

  /**
  * @namespace BrowserAllController
  */
  function BrowserAllController($location, $scope, Project) {
    var vm = this;
    vm.loadProject = loadProject;

    activate();

    /**
    * @name activate
    * @desc Actions to be performed upon instantiation of this controller
    */
    function activate() {
      Project.all().then(successFn);

      /**
      * @name successFn
      * @desc Populates the projects array
      */
      function successFn(data, status, headers, config) {
        vm.projects = Project.projects;
      }
    }

    /**
    * @name loadProject
    * @desc Retrieve a project and redirect to the browser view
    */
    function loadProject(id) {
      Project.get(id).then(successFn);

      /**
      * @name successFn
      * @desc Redirects to the browser view
      */
      function successFn(data, status, headers, config) {
        $location.path('/browser');
      }
    }
  }
})();
