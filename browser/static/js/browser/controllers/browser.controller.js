(function() {
  'use strict';

  angular
    .module('browser.controllers')
    .controller('BrowserController', BrowserController);

  BrowserController.$inject = ['$location', '$scope', 'Project', 'ProjectFile', 'Symbol'];

  /**
  * @namespace BrowserController
  */
  function BrowserController($location, $scope, Project, ProjectFile, Symbol) {
    var vm = this;
    vm.loadFile = loadFile;
    vm.findSymbol = findSymbol;

    activate();

    /**
    * @name activate
    * @desc Actions to be performed upon instantiation of this controller
    */
    function activate() {
      vm.project = Project.current;
      vm.file = vm.project.files[0];
      vm.searchval = '';

      loadFile(vm.file);

      $scope.$on('browser-symbol-click', loadFileAt);
    }

    /**
    * @name loadFile
    * @desc Try to retrieve a file and populate the browser text area
    */
    function loadFile(file) {
      ProjectFile.getContent(file.id).then(successFn);

      /**
      * @name successFn
      * @desc Populate the browser text area
      */
      function successFn(data, status, headers, config) {
        vm.file = file;
        vm.content = ProjectFile.content;
      }
    }

    /**
    * @name loadFileAt
    * @param file File to load
    * @param line Line number to jump to
    * @desc Loads the file 'file' and jumps to line number 'lineno'.
    */
    function loadFileAt(event, file, line) {
      loadFile(file);

      // Jump to line. To do this we basically just guess.
      //  1. Calculate the total number of lines of code
      var totalLines = vm.content.split('\n').length;

      // 2. Determine the approx fraction of the <pre> height at which our
      //    destination resides and hence the pixel location
      var fraction = line / totalLines;
      var position = ($('code').height() * fraction) + $('code').offset().top;

      // Scroll up a tiny bit just so we aren't pinned right to the top.
      position -= 20;

      // 3. Scroll to 'about' the location of the line
      $('html, body').animate({ scrollTop: position + 'px' }, 'slow');
    }



    /*
     * @name findSymbol
     * @param keyEvent The keypress event  
     * @desc On enter, lookup a symbol and on success jump to it in the browser 
     *       window
     */
    function findSymbol(keyEvent, sym) {
      console.log('Keypress: { keyEvent: ' + keyEvent + ', sym: ' + sym + ' }');
   
      // Only care about 'enter' presses
      if (keyEvent.which != 13) {
        return;
      }
    
      Symbol.search(vm.project.id, sym).then(successFn); 
    
      /*
       * @name successFn
       * @desc Load a the result of the symbol search
       */
      function successFn() {
        loadFileAt(null, Symbol.file, Symbol.line);
      }
    }
  }
})();
