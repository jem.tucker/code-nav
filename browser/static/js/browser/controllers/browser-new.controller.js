(function() {
  'use strict';

  angular
    .module('browser.controllers')
    .controller('BrowserNewController', BrowserNewController);

  BrowserNewController.$inject = ['$location', '$scope', 'Project'];

  /**
  * @namespace BrowserNewController
  */
  function BrowserNewController($location, $scope, Project) {
    var vm = this;
    vm.newProject = newProject;

    /**
    * @name newProject
    * @desc Create a new project and on succes redirect to /
    */
    function newProject() {
      Project.create(vm.url).then(successFn);

      /**
      * @name successFn
      * @desc Redirect to /
      */
      function successFn() {
        window.location = '/';
      }
    }
  }
})();
