(function () {
  'use strict';

  angular
    .module('browser.controllers')
    .controller('PrismController', PrismController);

  PrismController.$inject = ['$scope', '$element', 'Symbol', 'Project'];

  /**
  * @namespace PrismController
  */
  function PrismController($scope, $element, Symbol, Project) {
    var vm = this;

    activate();

    /**
    * @name activate
    * @desc Actions to be performed when this controller is instantiated
    */
    function activate() {
      $scope.$watch('source', render);
    }

    /**
    * @name render
    * @param {string} current The current value of `vm.source`
    * @param {string} original The value of `vm.source` before it was updated
    * @desc Re-highlight all code using prism asynchronously
    */
    function render(current, original) {
      if(current !== original) {
        // Re-highlight the code block asynchronously as the DOM will not have
        // been loaded from the template quite yet.
        $scope.$evalAsync(doRender);
      }

      /**
      * @name doRender
      * @desc Re-highlight code and setup click handlers for symbols
      */
      function doRender() {
        // Hilight code
        Prism.highlightAll("code");

        // Add a bunch of click handlers to the symbols
        var symbols = $element.find('span.token.function');
        symbols.on('click', function () {
          var project = Project.current;
          var sym = $(this)[0].innerHTML;
          Symbol.search(project.id, sym).then(successFn);

          /**
          * @name successFn
          * @desc Emit the 'browser-symbol-click' event with symbol file deets
          */
          function successFn(data, status, headers, config) {
            // Get the file
            var file = Symbol.file;
            var line = Symbol.line;

            // Emit an event for the BrowserController to handle by reloading
            // the source code area and jumping to the required line
            $scope.$emit('browser-symbol-click', file, line);
          }

        });
      }
    }
  }
})();
