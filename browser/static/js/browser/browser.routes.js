(function () {
  'use strict';

  angular
    .module('browser.routes')
    .config(config);

  config.$inject = ['$routeProvider'];

  /**
  * @name config
  * @desc Define valid application routes
  */
  function config($routeProvider) {
    $routeProvider.when('/browser-new', {
      controller: 'BrowserNewController',
      controllerAs: 'vm',
      templateUrl: '/static/templates/browser/browser-new.html'
    }).when('/browser-all', {
      controller: 'BrowserAllController',
      controllerAs: 'vm',
      templateUrl: '/static/templates/browser/browser-all.html'
    }).when('/browser', {
      controller: 'BrowserController',
      controllerAs: 'vm',
      templateUrl: '/static/templates/browser/browser.html'
    }).otherwise('/browser-all');
  }
})();
