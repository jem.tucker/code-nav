from django.core.management.base import BaseCommand, CommandError

from browser.models import Project 


class Command(BaseCommand):
    help = 'Load a project into the database'

    def add_arguments(self, parser):
        parser.add_argument('url', type=str)

    def handle(self, *args, **options):
        Project.load(options['url'])


