from django.db import models
from urllib.parse import urlparse
from os import path
from polo.settings import BASE_DIR

import logging


class Project(models.Model):
    """
    A repository avalible to be browsed
    """
    repository_name = models.CharField(max_length=255, blank=False)
    repository_path = models.CharField(max_length=255, blank=False)
    repository_url = models.CharField(max_length=255, unique=True, blank=False)

    @classmethod
    def load(cls, repository_url):
        """
        Load a repository by either cloning and indexing a new one or returning
        a pre-loaded instance
        @param repository_url: Url to a git repository. Only https is supported.
        @return: A new or pre-existing Project instance with repository_url
        """
        url = urlparse(repository_url)
        if url.scheme != 'https':
            raise Exception('Unsupported URL scheme')

        (repo, ext) = path.splitext(path.basename(url.path))
        if ext != '.git':
            raise Exception('Not a git repository')

        # Create the database record before we try and load.
        proj, created = cls.objects.get_or_create(
            repository_name=repo,
            repository_path=path.join(BASE_DIR, 'projects', repo),
            repository_url=repository_url
        )

        # If new we need to index the project so we can populate our files
        if created:
            logging.info('New project created: %s', proj.repository_name)
            from .helpers.indexed_project import IndexedProject
            IndexedProject(proj)
        else:
            logging.warning(
                'Attempt to load a project which already exists: %s',
                proj.repository_name)
        return proj

    def as_dict(self):
        """
        Returns this project in JSON form
        @return: A JSON string
        """
        data = {}
        data['id'] = self.id
        data['name'] = self.repository_name
        data['url'] = self.repository_url
        data['files'] = [f.as_dict() for f in self.files()]
        return data

    def files(self):
        """
        Get all ProjectFiles within this project
        @return: Query set of ProjectFile instances
        """
        return ProjectFile.objects.filter(project=self)


class ProjectFile(models.Model):
    """
    Single file within a project 
    """
    name = models.CharField(max_length=255, blank=False)
    path = models.CharField(max_length=255, blank=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    # Base64 encoded gzipped text
    data = models.TextField(blank=True)
     
    def as_dict(self):
        """
        Returns this file metadata in JSON form
        @return: A JSON string
        """
        data = {}
        data['id'] = self.id
        data['name'] = self.path
        return data


class Symbol(models.Model):
    """
    A symbol within a file. For example a function name. 
    """
    name = models.CharField(max_length=1024, blank=False)
    line = models.IntegerField(null=False)
    proj_file = models.ForeignKey(ProjectFile, null=False)
    symtype = models.IntegerField(null=False)

    @classmethod
    def find(cls, project, name):
        """
        Lookup a symbol by name and project t
        """
        files = ProjectFile.objects.filter(project=project)
        symbols = cls.objects.filter(proj_file__in=files, name=name)
        count = symbols.count()
        if count is 1:
            return symbols.first()
        elif count > 1:
            import teager
            return symbols.filter(symtype=teager.TYPE_DEFINITION).first()
        else:
            return None


