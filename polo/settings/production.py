from polo.settings.base import *

import os
import dj_database_url


SECRET_KEY = os.environ['SECRET_KEY']

db_from_env = dj_database_url.config()
DATABASES['default'].update(db_from_env)
