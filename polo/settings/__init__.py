from polo.settings.base import *

if DEBUG:
    from polo.settings.debug import *
else:
    from polo.settings.production import *
