from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='browser/index.html')),
    url(r'^about/$', TemplateView.as_view(template_name='about.html')),
    url(r'^browser/', include('browser.urls'))
]
